package com.flyAway.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Flights {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int flightId;
	
	@Column(name="TRAVEL_DATE")
	private String travelDate;
	
	@Column(name="DEPARTURE_TIME")
	private String departureTime;
	
	@Column(name="ARRIVAL_TIME")
	private String arrivalTime;
	
	@Column(name="SEATS_REMAINING")
	private int remainingSeats;
	
	//Many to One Mapping to Place
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="DEPARTURE_PLACE")
	private Place departure;
	
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ARRIVAL_PLACE")
	private Place source;
	
	//Many to One mapping to Airline
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="AIRLINE")
	private Airline airline;

	public Flights() {
		
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getTravelDate() {
		return travelDate;
	}

	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getRemainingSeats() {
		return remainingSeats;
	}

	public void setRemainingSeats(int remainingSeats) {
		this.remainingSeats = remainingSeats;
	}

	public Place getDeparture() {
		return departure;
	}

	public void setDeparture(Place departure) {
		this.departure = departure;
	}

	public Place getSource() {
		return source;
	}

	public void setSource(Place source) {
		this.source = source;
	}

	public Airline getAirline() {
		return airline;
	}

	public void setAirline(Airline airline) {
		this.airline = airline;
	}

	@Override
	public String toString() {
		return "Flights [travelDate=" + travelDate + ", departureTime=" + departureTime + ", arrivalTime=" + arrivalTime
				+ ", remainingSeats=" + remainingSeats + ", departure=" + departure + ", source=" + source
				+ ", airline=" + airline + "]";
	}

	
	}
	
	
	
	
	


