package com.flyAway.entity.dao;

import java.util.List;

import org.hibernate.Session;

import com.flyAway.entity.Airport;

public interface AirportDAO {
	public Airport addAirport(Airport bean);

	public Airport addAirport(Session session, Airport bean);

	public int deleteAirport(int id);

	public List<AirportDAO> getAirports();

	public int updateAirport(int id, Airport airport);

	public void setCountryIsoCode(String countryIsoCode);

	public void setIataCode(String iataCode);

	public void setName(String name);

}
