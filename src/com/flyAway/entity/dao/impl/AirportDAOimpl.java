package com.flyAway.entity.dao.impl;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.flyAway.entity.Airport;
import com.flyAway.entity.dao.AirportDAO;
import com.flyAway.entity.util.SessionUtil;


public class AirportDAOimpl implements AirportDAO {
	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	
		@Override
	        public List<AirportDAO> getAirports(){
	        Session session = SessionUtil.getSession();    
	        Query query = session.createQuery("from Airport");
	        List<AirportDAO> airports =  query.list();
	        session.close();
	        return airports;
	    }
	 
		@Override
			public Airport addAirport(Airport bean){
			  Session session = SessionUtil.getSession();        
		        Transaction tx = session.beginTransaction();
		        addAirport(session,bean);        
		        tx.commit();
		        session.close();
				return bean;

			}
		

		@Override
		public Airport addAirport(Session session, Airport bean) {
			    	AirportDAOimpl airport = new AirportDAOimpl();
			        airport.setCountryIsoCode(bean.getCountryIsoCode());
			        airport.setIataCode(bean.getIataCode());
			        airport.setName(bean.getName());        
			        session.save(airport);
					return null;
		}

		@Override
		public int deleteAirport(int id) {
			  Session session = SessionUtil.getSession();
		        Transaction tx = session.beginTransaction();
		        String hql = "delete from Airport where id = :id";
		        Query query = session.createQuery(hql);
		        query.setInteger("id",id);
		        int rowCount = query.executeUpdate();
		        System.out.println("Rows affected: " + rowCount);
		        tx.commit();
		        session.close();
		        return rowCount;
		}

		@Override
		public int updateAirport(int id, Airport airport) {
			if(id<=0)
				return 0;
		     
	         Session session = SessionUtil.getSession();
	            Transaction tx = session.beginTransaction();
	            String hql = "update Airport set name:=name, countryIsoCode:=countryIsoCode, iataCode:=iataCode where id:=id";
	            Query query = session.createQuery(hql);
	            
	            query.setInteger("id",id);
	            query.setString("iataCode",airport.getCountryIsoCode());
	            query.setString("countryIsoCode",airport.getIataCode());
	            query.setString("name",airport.getName());
	            int rowCount = query.executeUpdate();
	            System.out.println("Rows affected: " + rowCount);
	            tx.commit();
	            session.close();
				return rowCount;
	                        
		}
		
		public void setName(String name) {
			// TODO Auto-generated method stub
			
		}

		public void setIataCode(String iataCode) {
			// TODO Auto-generated method stub
			
		}

		public void setCountryIsoCode(String countryIsoCode) {
			// TODO Auto-generated method stub
			
		}

		
			
			
		}
	



