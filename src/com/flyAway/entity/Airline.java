package com.flyAway.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Airline {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int airlineId;
	private String airlineName;
	private int seatCapacity;
	
	@OneToMany(mappedBy="airline")
	private List<Flights>flight;
	
	
	public Airline() {
		super();
	}

	public int getAirlineId() {
		return airlineId;
	}

	public void setAirlineId(int airlineId) {
		this.airlineId = airlineId;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public int getSeatCapacity() {
		return seatCapacity;
	}

	public void setSeatCapacity(int seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	
	public List<Flights> getFlightDetails(){
		return flight;
		
	}
	
	public void setFlightDetails(List<Flights>flight) {
	this.flight=flight;
	}

	@Override
	public String toString() {
		return "Airline [airlineName=" + airlineName + ", seatCapacity=" + seatCapacity + ", flight=" + flight + "]";
	}
	
	
	
	}
	

