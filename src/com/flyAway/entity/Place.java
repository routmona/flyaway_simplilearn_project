package com.flyAway.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Place {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int placeId;
	
	@Column(name="AIRPORT_CODE")
	private String airportCode;
	
	@Column(name="AIRPORT_NAME")
	private String airportName;
	
	@OneToMany(mappedBy="source")
	private List<Flights>flight1;
	
	@OneToMany(mappedBy="departure")
	private List<Flights>flight2;

	public int getPlaceId() {
		return placeId;
	}

	public void setPlaceId(int placeId) {
		this.placeId = placeId;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public List<Flights> getFlight1() {
		return flight1;
	}

	public void setFlight1(List<Flights> flight1) {
		this.flight1 = flight1;
	}

	public List<Flights> getFlight2() {
		return flight2;
	}

	public void setFlight2(List<Flights> flight2) {
		this.flight2 = flight2;
	}

	@Override
	public String toString() {
		return "Place [airportCode=" + airportCode + ", airportName=" + airportName + ", flight1=" + flight1
				+ ", flight2=" + flight2 + "]";
	}
	

	
	
	}
	
	
	
	
	
	
	


