package com.flyAway.entity.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.flyAway.entity.Airport;
import com.flyAway.entity.Flights;
import com.flyAway.entity.Passenger;

public class SessionUtil {

	private static SessionUtil instance = new SessionUtil();

	private static SessionFactory sessionFactory;

	public static SessionUtil getInstance() {
		return instance;
	}

	private SessionUtil() {
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml").addAnnotatedClass(Airport.class).addAnnotatedClass(Flights.class)
				.addAnnotatedClass(Passenger.class);

	}

	public static Session getSession() {
		Session session = getInstance().sessionFactory.openSession();
		return session;
	}
}
